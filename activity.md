1. Changing the selected region in the management console will not affect the listed instances in your account.
```
False
```

2. What is the meaning of PEM in a PEM file?
```
Privacy Enhanced Mail
```

3. How many instance types does Amazon EC2 service have?
```
5
```

4. AWS stands for?
```
Amazon Web Services
```

5. VM stands for?
```
Virtual Machine
```

6. What is the name of the instance type we used to create a new instance?
```
t2.micro
```

7. Refers to on-demand availability of computing-related resources such as servers, storage and databases without direct hardware management by the user.
```
Cloud computing
```

8. Contains a variety of services that can be used together in order to quickly build and deploy applications.
```
Cloud computing platform
```

9. Refers to a service (specifically, Platform as a Service or PaaS) offered to businesses both large and small.
```
Cloud computing platform
```

10. What is an alternative term for virtual machine within your own machine?
```
Guest
```

11. It is a term used within AWS to refer to virtual machines.
```
Instance
```

12. How many total regions do AWS offer in its EC2 service?
```
31
```